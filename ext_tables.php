<?php
defined('TYPO3_MODE') or die();

// Include new content elements to modWizards
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:boto_slider/Configuration/PageTSconfig/BotoSlider.ts">'
);

// Register for hook to show preview of tt_content element of CType="boto_slider" in page module
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['boto_slider']
    = \Bosshartong\BotoSlider\Hooks\BotoSliderPreviewRenderer::class;

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Boto Fluid Styled Slider');

// Add a flexform to the boto_slider CType
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '',
    'FILE:EXT:boto_slider/Configuration/FlexForms/boto_slider_flexform.xml',
    'boto_slider'
);
