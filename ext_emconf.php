<?php

$EM_CONF[$_EXTKEY] = array(
    'title'              => 'BossharTong: Fluid Styled Slider',
    'description'        => 'A slider Content Element based on fluid_styled_content.',
    'category'           => 'plugin',
    'version'            => '1.0.2',
    'shy'                => '',
    'priority'           => '',
    'loadOrder'          => '',
    'module'             => '',
    'state'              => 'stable',
    'uploadfolder'       => 0,
    'createDirs'         => '',
    'modify_tables'      => '',
    'clearcacheonload'   => 1,
    'lockType'           => '',
    'author'             => 'Nando Bosshart',
    'author_email'       => 'typo3@bosshartong.ch',
    'author_company'     => 'BossharTong GmbH',
    'CGLcompliance'      => null,
    'CGLcompliance_note' => null,
    'constraints'        => array(
        'depends' => array(
            'extbase' => '8.7',
            'fluid' => '8.7',
            'typo3' => '8.7',
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
    'autoload' => array(
        'psr-4' => array(
            'Bosshartong\\BotoSlider\\' => 'Classes',
        ),
    ),
);
