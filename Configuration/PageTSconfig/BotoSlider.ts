# **************************************************
# Add the slider to the "New Content Element Wizard"
# **************************************************
mod.wizards.newContentElement.wizardItems.common {
	elements {
		boto_slider {
			iconIdentifier = content-image
			title = LLL:EXT:boto_slider/Resources/Private/Language/locallang_be.xlf:wizard.title
			description = LLL:EXT:boto_slider/Resources/Private/Language/locallang_be.xlf:wizard.description
			tt_content_defValues {
				CType = boto_slider
			}
		}
	}
	show := addToList(boto_slider)
}